# Simplegabor - Multiresolution Gabor Feature Toolbox

## Description

This package contains a Matlab toolbox for multiresolution Gabor filtering of 2-D signals (images). The implementation is intended to be as efficient as possible within limitations of Matlab. This toolbox replaces an [older Gabor filtering toolbox](https://www.it.lut.fi/project/gabor/downloads/src/gabortb/).

Version 0.2.2 includes only frequency domain filtering. Spatial domain filtering would be faster in some cases, namely when filter responses are needed only for some points and not for all of the image. As of version 0.2.1 the toolbox no longer requires [statistics toolbox](https://www.mathworks.com/products/statistics.html).


## Authors

Jarmo Ilonen and [Joni Kämäräinen](http://vision.cs.tut.fi/personal/JoniKamarainen/).

## Copyright

The Simplegabor Toolbox is Copyright © 2006 by Jarmo Ilonen and Joni Kämäräinen.

The software package is free software; you can redistribute it and/or modify it under terms of [GNU General Public License](https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html) as published by the Free Software Foundation; either version 2 of the license, or any later version. For more details see licenses at www.gnu.org

The software package is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

As stated in the GNU General Public License it is not possible to include this software library or even parts of it in a proprietary program without written permission from the owners of the copyright. If you wish to obtain such permission, you can reach us by mail:

      Department of Information Processing
      Lappeenranta University of Technology
      P.O. Box 20 FIN-53851 Lappeenranta
      FINLAND

and by e-mail:

      
      jarmo.ilonen@lut.fi
      joni.kamarainen@lut.fi
If you find any bugs, please contact the authors.

## References

Simple Garbor original source is/was available at [http://www2.it.lut.fi/project/simplegabor/downloads/src/simplegabortb/](http://www2.it.lut.fi/project/simplegabor/downloads/src/simplegabortb/).

